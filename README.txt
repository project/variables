----------------------------
Overview of variables.module
----------------------------

Module for handling all the variables on a Drupal site, inspired by the mozilla about:config

------
Author
------

Lars S. Geisler (http://www.larssg.com/)
Sven Pedersen (http://www.trioglobal.com/)

------------------
README.txt version
------------------
